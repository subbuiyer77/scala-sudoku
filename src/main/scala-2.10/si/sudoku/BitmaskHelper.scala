package si.sudoku

import scala.collection.mutable.ListBuffer

object BitmaskHelper {

  implicit class IntList2BitmaskedInt(val numbers: List[Int]) extends AnyVal {
    def toBitmask: Int = {
      numbers.map(number => number.toBitmask).foldLeft(0)((a,b)=>a | b)
    }
  }

  implicit class CellKey2Coordinates(val cellKey: String) extends AnyVal {
    def toCoordinates: Tuple3[Char, Int, String] = (toColLabel, toRowLabel, toSquareLabel)
    def toColLabel: Char = cellKey.charAt(0)
    def toRowLabel: Int  = cellKey.charAt(1).asDigit

    def toColIndex: Int = cellKey.charAt(0).toInt - 65
    def toRowIndex: Int = cellKey.charAt(1).asDigit - 1

    def toSquareLabel: String = {
      val part1 = cellKey toRowLabel match {
        case x if 1 to 3 contains x => "0"
        case x if 4 to 6 contains x => "1"
        case x if 7 to 9 contains x => "2"
      }
      val part2 = cellKey toColLabel match {
        case x if 'A' to 'C' contains x => "0"
        case x if 'D' to 'F' contains x => "1"
        case x if 'G' to 'I' contains x => "2"
      }
      s"S$part1$part2"
    }
  }

  implicit class Coordinates2CellKey(val coordinates: Tuple2[Int, Int]) extends AnyVal {
    def toCellKey: String = {
      s"${(coordinates._2 + 65).toChar}${coordinates._1 + 1}"
    }
  }



  implicit class Int2BitmaskedInt(val n: Int) extends AnyVal {
    def toBitmask: Int = n match {
      //case 0 => 0   //Integer.parseInt("000000000", 2); //0
      case 1 => 256 //Integer.parseInt("100000000", 2); //256
      case 2 => 128 //Integer.parseInt("010000000", 2); //128
      case 3 => 64  //Integer.parseInt("001000000", 2); //64
      case 4 => 32  //Integer.parseInt("000100000", 2); //32
      case 5 => 16  //Integer.parseInt("000010000", 2); //16
      case 6 => 8   //Integer.parseInt("000001000", 2); //8
      case 7 => 4   //Integer.parseInt("000000100", 2); //4
      case 8 => 2   //Integer.parseInt("000000010", 2); //2
      case 9 => 1   //Integer.parseInt("000000001", 2); //1
      case _ => throw new UnsupportedOperationException("Cannot bitmask numbers other than 1 through 9")
    }

    def fromBitmask: List[Int] = {
      val numbers = new ListBuffer[Int]();
      for(i<- (0 to 8).reverse){
        val mask = (1 << i)

        if ((n & mask) == mask){
          numbers.append(9 - i)
        }
      }
      numbers.toList
    }
  }
}
