

Integer.parseInt("100000000", 2)
Integer.parseInt("001000000", 2)
Integer.parseInt("101000000", 2)
Integer.parseInt("111111111", 2)
//target inversion
Integer.parseInt("010111111", 2)
256 | 64
511 ^ 320
//options based on row
val rowOptions = Integer.parseInt("010111111", 2)
// column values
Integer.parseInt("001000000", 2)
Integer.parseInt("000010000", 2)
64 | 16
Integer.parseInt("001010000", 2)
//invert column values
val colOptions = 511  ^ 80
Integer.parseInt("110101111", 2)
// final options should be 010101111
Integer.parseInt("010101111", 2)

//options based on row and column
val rowColOptions = 191 & 431
(256 & (1<<8)) == 256
val squareIds = List[String]("00", "01", "02", "10", "11", "12", "20", "21", "22")
val squareId2s = List("00", "01", "02", "10", "11", "12", "20", "21", "22")
val squareId3s = ("00", "01", "02", "10", "11", "12", "20", "21", "22")
0 to 8

for(col <-  'A' to 'I'){
  println(col)
}

for (colLabel <- 'A' to 'C') {
  println(s"${colLabel.toInt}")

}

65.toChar
