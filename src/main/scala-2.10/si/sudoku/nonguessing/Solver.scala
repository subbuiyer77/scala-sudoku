package si.sudoku.nonguessing

import si.sudoku.BitmaskHelper._

import scala.annotation.tailrec
import scala.collection.mutable
import scala.io.Source

case class CellValue(col: Char, row: Int, square: String, var valueType: String, var valuesBitMask: Int)
case class CellSolution(key:String, solution:Int)

object Solver {
  def main(args: Array[String]) {
    val squareIds = List ( "00", "01", "02", "10", "11", "12", "20", "21", "22" )
    val cellsMap = new mutable.HashMap[String, CellValue]
    val cellsArray = new Array[Array[CellValue]](9)
    //initialize the sudoku array
    for (row <- 0 to 8) {
      val rowLabel = row + 1;

      cellsArray(row) = new Array[CellValue](9)
      for (col <- 0 to 8) {
        // we want column labels to be A through I
        val colLabel = (col + 65).toChar
        val squareId = row / 3 + "" + col / 3
        val cell = CellValue(colLabel, rowLabel, squareId, "possible", 0)
        cellsMap.put(colLabel+""+ rowLabel, cell)
        cellsArray(row)(col) = cell
      }
    }

    //now fill it with provided game data
    val reader = Source.fromFile("c:/Users/subbu/Code/scala/scala-sudoku/src/main/resources/noguess.sdk")
    val lines = reader.getLines()

    for (row <- 0 to 8) {
      if (lines.hasNext) {
        val line = lines.next
        for (col<- 0 to 8) {
          if (line.length > col && ' ' != line.charAt(col)) {
            val cell: CellValue = cellsArray(row)(col)
            cell.valuesBitMask = line.charAt(col).toString.toInt.toBitmask
            cell.valueType = "given"
          }
        }
      }
    }
    reader.close()
    // now that the game data has been read in, print out the game
    prettyPrint(cellsArray)

    def calculateOptionsForSquare(solvedCells: Iterable[CellValue], squareId: String): Int = {
      solvedCells.filter(filterBySquare(squareId)).map(value => value.valuesBitMask).foldLeft(Integer.parseInt("111111111", 2))((a, b) => a ^ b)
    }
    def calculateOptionsForColumn(solvedCells: Iterable[CellValue], colNum: Int): Int = {
      solvedCells.filter(filterByColumn(colNum)).map(value => value.valuesBitMask).foldLeft(Integer.parseInt("111111111", 2))((a, b) => a ^ b)
    }
    def calculateOptionsForRow(solvedCells: Iterable[CellValue], rowNum: Int): Int = {
      solvedCells.filter(filterByRow(rowNum)).map(value => value.valuesBitMask).foldLeft(Integer.parseInt("111111111", 2))((a, b) => a ^ b)
    }
    @tailrec
    def solve: Unit = {
      val solvedCells = cellsMap.values.filter(cell => cell.valueType.matches("given|solved"))

      val unsolvedCells = cellsMap.values.filter(cell => cell.valueType == "possible").toList

      if(unsolvedCells.length == 0){
        println(s"Puzzle fully solved...")
        return
      }

      println(s"Unsolved cell count is ${unsolvedCells.length }")
      // get a KV pair of rowNum to rowOptions
      val optionsByRow = (1 to 9).map(rowNum => (rowNum, calculateOptionsForRow(solvedCells, rowNum))).toMap
      val optionsByCol = ('A' to 'I').map(colNum => (colNum, calculateOptionsForColumn(solvedCells, colNum))).toMap
      val optionsBySquare = squareIds.map(squareId => (squareId, calculateOptionsForSquare(solvedCells, squareId))).toMap

      //update options for each unsolved cell
      unsolvedCells.foreach(unsolvedCell => {
        // get list of non-options
        val rowOptions = optionsByRow(unsolvedCell.row)
        val colOptions = optionsByCol(unsolvedCell.col)
        val squareOptions = optionsBySquare(unsolvedCell.square)
        val finalOptions = rowOptions & colOptions & squareOptions
        unsolvedCell.valuesBitMask = finalOptions
      })

      val cellSolutions: List[CellSolution] = unsolvedCells.map(unsolvedCell => findSolution(unsolvedCell, cellsMap)).filter(_.solution!=0)

      if(cellSolutions.length == 0){
        println("cannot solve anything more")
        return
      }

      cellSolutions.foreach(solution => {
        val cell: CellValue = cellsMap.get(solution.key).get
        cell.valueType = "solved"
        cell.valuesBitMask = solution.solution.toBitmask
      })
      printWorkingGrid(cellsArray)
      solve
    }

    // now solve the grid
    // for each cell eliminate invalid options
    solve
    prettyPrint(cellsArray)
  }

  def findSolution(unsolvedCell: CellValue, cellsMap: mutable.HashMap[String, CellValue]) : CellSolution = {
    if (unsolvedCell.valuesBitMask.fromBitmask.length == 1) {
      val solution: Int = unsolvedCell.valuesBitMask.fromBitmask(0)
      println(s"Cell ${unsolvedCell.col}${unsolvedCell.row} can be solved to $solution using 'last man standing'")
      return CellSolution(unsolvedCell.col+""+unsolvedCell.row, solution)
    }

    //next: check for alone among friends
    for (i <- unsolvedCell.valuesBitMask.fromBitmask) {
      //find how many times each number option occurs within each square
      //if the number is 1, then the cell must contain that number.
      val iBitMask = 1 << (9 - i);
      val count = cellsMap.values
        .filter(cell => cell.square == unsolvedCell.square && cell.valueType.matches("possible"))
        .count(cell => (cell.valuesBitMask & iBitMask) == iBitMask)

      if (count == 1) {
        println(s"Cell ${unsolvedCell.col}${unsolvedCell.row} can be solved to $i using 'alone among friends'")
        return CellSolution(unsolvedCell.col+""+unsolvedCell.row, i)
      }
    }
    CellSolution(unsolvedCell.col+""+unsolvedCell.row, 0)
  }

  def filterBySquare(squareId: String): (CellValue) => Boolean = {
    cell => cell.square == squareId
  }

  def filterByRow(rowNum: Int): (CellValue) => Boolean = {
    cell => cell.row == rowNum
  }

  def filterByColumn(colNum: Int): (CellValue) => Boolean = {
    cell => cell.col == colNum
  }

  def printWorkingGrid(cells: Array[Array[CellValue]]): Unit = {
    println
    println("           A            B            C            D            E            F            G            H            I")
    println("--------------------------------------------------------------------------------------------------------------------------")
    for (r <- 0 to 8) {
      print(s" ${r+1}  |")
      for (c <- 0 to 8) {
        val cell = cells(r)(c)
        print(cell.valueType match {
          case "given" => "G: " + printNumbers(cell)
          case "possible" => "P: " + printNumbers(cell)
          case "solved" => "S: " + printNumbers(cell)
          case _ => "            "
        })

        if ((c + 1) % 3 == 0)
          print("|")
        else
          print(",")

      }
      println()
      if ((r + 1) % 3 == 0)
        println("--------------------------------------------------------------------------------------------------------------------------")
    }
    println()

    def printNumbers(cell: CellValue): String = {
      val values = cell.valuesBitMask.fromBitmask
      val strb = new StringBuilder();
      for (i <- 1 to 9) {
        if (values.contains(i))
          strb.append(i)
        else
          strb.append(' ')
      }
      strb.toString()
    }
  }

  def prettyPrint(cells: Array[Array[CellValue]]): Unit = {
    println
    println("      A   B   C   D   E   F   G   H   I")
    println("-----------------------------------------")
    for (r <- 0 to 8) {
      print(s" ${r + 1}  |")
      for (c <- 0 to 8) {
        val cell = cells(r)(c)
        if(cell.valuesBitMask.fromBitmask.length == 1)
          print(s" ${cell.valuesBitMask.fromBitmask(0)} " )
        else
          print("   ")
        if ((c + 1) % 3 == 0)
          print("|")
        else
          print(" ")

      }
      println()
      if ((r + 1) % 3 == 0)
        println("-----------------------------------------")
    }
    println()
  }
}
