package si.sudoku.guessing

import si.sudoku.BitmaskHelper._

import scala.annotation.tailrec
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.io.Source

// knowledgeCode can be c=>certain, u=>unknown, g=>guess
case class Cell(key: String, valueBitmap: Int, knowlCode: String, knowlReason: String){
  override def toString: String = s"Cell($key ${valueBitmap.fromBitmask} $knowlCode $knowlReason"
}

object SolverV2 {
  val squareIds = List("S00", "S01", "S02", "S10", "S11", "S12", "S20", "S21", "S22")

  def main(args: Array[String]) {
    val allCells: List[Cell] = getAllCells(loadGame())
    println(allCells.filter(_.knowlCode=="c").map(cell=> s"${cell.key}=${cell.valueBitmap.fromBitmask(0)}").mkString(","))
    prettyPrint(allCells)
    val solution = solve(allCells)
    prettyPrint(solution)
    println(solution.filter(_.knowlCode=="c").map(cell=> s"${cell.key}=${cell.valueBitmap.fromBitmask(0)}").mkString(","))
  }

  def solve(allCells: List[Cell]): List[Cell] = {
    @tailrec
    def solve(certainCells: List[Cell], unknownCells: List[Cell]): List[Cell] = {
      println(s"In this iteration, there are ${unknownCells.length} unsolved cells...")
      // update list of options for each unsolved cell
      val unsolvedCellsWithUpdatedOptions: List[Cell] = updateOptions(certainCells, unknownCells)

      // look for naked singles
      var newlySolvedCells = unsolvedCellsWithUpdatedOptions.filter(_.valueBitmap.fromBitmask.length == 1).map(cell=>Cell(cell.key, cell.valueBitmap, "c", "naked single"))

      if(newlySolvedCells.length == 0){
        //look for hidden singles
        newlySolvedCells = unsolvedCellsWithUpdatedOptions.map(unsolvedCell => {
          var solvedCell = unsolvedCell
          for (i <- unsolvedCell.valueBitmap.fromBitmask) {
            //find how many times each number option occurs within each square
            //if the number is 1, then the cell must contain that number.
            val iBitMask = 1 << (9 - i)

            val count = unsolvedCellsWithUpdatedOptions
              .filter(_.key.toSquareLabel == unsolvedCell.key.toSquareLabel) //get all unsolved cells in square
              .count(cell => (cell.valueBitmap & iBitMask) == iBitMask)  // get count

            if (count == 1) {
              solvedCell = Cell(unsolvedCell.key, i.toBitmask, "c", "hidden single")
            }
          }
          solvedCell
        }).filter(_.knowlCode == "c")
      }

      if(newlySolvedCells.length > 0){
        println(s"The following cells are solvable with certainity")
        newlySolvedCells.foreach(println)
      }

      val newlySolvedCellKeys = newlySolvedCells.map(_.key)
      val remainingUnsolvedCells = unsolvedCellsWithUpdatedOptions.filterNot(cell => newlySolvedCellKeys.contains(cell.key) )

      if (newlySolvedCells.length == 0) {
        println("cannot solve anymore ...")
        //return complete state of the solution
        return certainCells ::: remainingUnsolvedCells
      }
      if (certainCells.length + newlySolvedCells.length == 81) {
        println()
        println("All cells solved...Exiting the Solve function")
        //return complete state of the solution
        return certainCells ::: newlySolvedCells
      }
//      prettyPrint(certainCells:::newlySolvedCells:::remainingUnsolvedCells)
      // make a tail recursive call to solve remaining unsolved cells.
      solve(certainCells:::newlySolvedCells, remainingUnsolvedCells)
    }
    solve(allCells.filter("c" == _.knowlCode), allCells.filter("u" == _.knowlCode))
  }

  def updateOptions(certainCells: List[Cell], unknownCells: List[Cell]): List[Cell] = {
    // isolate options for each row, column and square
    val optionsByRow = (1 to 9).map(rowLabel => (rowLabel, certainCells.filter(_.key.toRowLabel == rowLabel).map(_.valueBitmap).foldLeft(Integer.parseInt("111111111", 2))((a, b) => a ^ b))).toMap
    val optionsByCol = ('A' to 'I').map(colLabel => (colLabel, certainCells.filter(_.key.toColLabel == colLabel).map(_.valueBitmap).foldLeft(Integer.parseInt("111111111", 2))((a, b) => a ^ b))).toMap
    val optionsBySquare = squareIds.map(squareLabel => (squareLabel, certainCells.filter(_.key.toSquareLabel == squareLabel).map(_.valueBitmap).foldLeft(Integer.parseInt("111111111", 2))((a, b) => a ^ b))).toMap

    // compute options for each cell
    val unsolvedCellsWithUpdatedOptions = unknownCells.map(cell => Cell(cell.key, optionsByCol(cell.key.toColLabel) & optionsByRow(cell.key.toRowLabel) & optionsBySquare(cell.key.toSquareLabel), "u", ""))
    unsolvedCellsWithUpdatedOptions
  }

  def loadGame(): List[Cell] = {
    val cellsList = new ListBuffer[Cell]

    //      now fill it with provided game data
    val reader = Source.fromFile("c:/Users/subbu/Code/scala/scala-sudoku/src/main/resources/noguess.sdk")
    val lines = reader.getLines()

    for (row <- 0 to 8) {
      if (lines.hasNext) {
        val line = lines.next
        for (col <- 0 to 8) {
          val cellKey = (row, col).toCellKey
          if (line.length > col && ' ' != line.charAt(col)) {
            cellsList += Cell(cellKey, line.charAt(col).asDigit.toBitmask, "c", "given" )
          }
        }
      }
    }
    reader.close()
    //make it immutable when returning
    cellsList.toList
  }

  def getAllCells(certainCells: List[Cell]): List[Cell] = {
    val cellsMap = new mutable.HashMap[String, Cell]
    //initialize the sudoku array
    for (colLabel <- 'A' to 'I') {
      for (rowLabel <- 1 to 9) {
        val cellKey: String = s"$colLabel$rowLabel"
        cellsMap.put(cellKey, Cell(cellKey, 0, "u", ""))
      }
    }
    certainCells.foreach(cell=> cellsMap.put(cell.key, cell))
    cellsMap.values.toList
  }

  def prettyPrint(cells: List[Cell]): Unit = {
    val cellsArray = new Array[Array[Cell]](9)
    //initialize the sudoku array
    for (row <- 0 to 8) {
      cellsArray(row) = new Array[Cell](9)
    }

    cells.foreach(cell=>{
      cellsArray(cell.key.toRowIndex)(cell.key.toColIndex) = cell
    })

    println
    println("      A   B   C   D   E   F   G   H   I")
    println("-----------------------------------------       ----------------------------------------------------------------------------------------------------------------------------------------------------")
    for (r <- 0 to 8) {
      print(s" ${r + 1}  |")
      for (c <- 0 to 8) {
        val cell = cellsArray(r)(c)
        if(cell != null && cell.valueBitmap.fromBitmask.length == 1)
          print(s" ${cell.valueBitmap.fromBitmask(0)} ")
        else
          print("   ")

        if ((c + 1) % 3 == 0) print("|") else print(" ")
      }

      print(s"       ${r + 1}  |")
      for (c <- 0 to 8) {
        val cell:Cell = cellsArray(r)(c)


        if(cell != null && cell.valueBitmap.fromBitmask.length == 1){
          print(cell.knowlCode match {
            case "c" => "C: " + printNumbers(cell)
            case "u" => "U: " + printNumbers(cell)
//            case "solved" => "S: " + printNumbers(cell)
            case _ => "         "
          })
        }
        else
          print("            ")

        if ((c + 1) % 3 == 0) print("|") else print(" , ")
      }

      println()
      if ((r + 1) % 3 == 0)
        println("-----------------------------------------       ----------------------------------------------------------------------------------------------------------------------------------------------------")
    }
    println()
  }
  def printNumbers(cell: Cell): String = {
    val values = cell.valueBitmap.fromBitmask
    val strb = new StringBuilder();
    for (i <- 1 to 9) {
      if (values.contains(i))
        strb.append(i)
      else
        strb.append(' ')
    }
    strb.toString()
  }
}
