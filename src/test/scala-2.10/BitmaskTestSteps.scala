import cucumber.api.scala.{EN, ScalaDsl}
import org.junit.Assert._
import si.sudoku.BitmaskHelper._

class BitmaskTestSteps  extends ScalaDsl with EN {
  When("""^noop$"""){ () =>
  }

  Then("""^(.*) when bitmasked returns (.*)$"""){ (numbersListStr:String, bitPattern:String) =>
    val inputs = numbersListStr.split(",").map(str=>str.toInt).toList
    assertEquals(Integer.parseInt(bitPattern, 2), inputs.toBitmask)
  }

  Then("""^(.*) when unmasked returns (.*)$"""){ (bitPattern:String, numberListStr:String) =>
    assertEquals(numberListStr, Integer.parseInt(bitPattern, 2).fromBitmask.mkString(","))
  }

  Then("""^(.*) when bitmasked must throw an UnsupportedOperationException$"""){ (numbersListStr:String) =>
    try {
      val inputs = numbersListStr.split(",").map(str=>str.toInt).toList.toBitmask
    }
    catch {
      case uoe: UnsupportedOperationException => //do nothing as this is an expected  println(uoe.getMessage )
      case e: Exception => fail("An unexpected exception was generated: " + e.getMessage )
    }
  }
}
