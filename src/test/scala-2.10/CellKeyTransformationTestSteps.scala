import cucumber.api.scala.{EN, ScalaDsl}
import org.junit.Assert._
import si.sudoku.BitmaskHelper._

class CellKeyTransformationTestSteps extends ScalaDsl with EN {
  Then("""^(.*) yields labels (.*), (\d+) and (.*)$"""){ (cellKey:String, colLabel:Char, rowLabel:Int, squareLabel: String) =>
    val coordinates = cellKey.toCoordinates
    assertEquals(colLabel, coordinates._1)
    assertEquals(rowLabel, coordinates._2)
    assertEquals(squareLabel, coordinates._3)
  }

  Then("""^\((\d+), (\d+)\) coordinates yields (.*)$"""){ (rowIndex:Int, colIndex:Int, cellKey:String) =>
    assertEquals(cellKey, (rowIndex, colIndex).toCellKey)
  }

  Then("""^(.*) yields indices (\d+), (\d+)$"""){ (cellKey:String, rowIndex:Int, colIndex:Int) =>
    assertEquals(rowIndex, cellKey.toRowIndex)
    assertEquals(colIndex, cellKey.toColIndex)
  }
}
