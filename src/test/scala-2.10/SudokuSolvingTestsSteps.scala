import cucumber.api.scala.{EN, ScalaDsl}
import org.junit.Assert._
import si.sudoku.BitmaskHelper._
import si.sudoku.guessing.{Cell, SolverV2}

import scala.collection.mutable

class SudokuSolvingTestsSteps extends ScalaDsl with EN {
  var solution: List[Cell] = null

  When( """^cells (.*) are certain$""") { (certainCellData: String) =>
    val givenCells = certainCellData.split(",").map(t=> {
      val tokens = t.split("=")
      Cell(tokens(0).trim, tokens(1).trim.toInt.toBitmask, "c", "given")
    }).toList
    givenCells.foreach(println)
    solution = SolverV2.solve( SolverV2.getAllCells(givenCells))
  }

  Then("""^then the (.*) has (\d+) cells$"""){ (solutionStr: String, expectedSolvedCellCount:Int) =>
    val expectedSolutionsMap = new mutable.HashMap[String, Int]

    solutionStr.split(",").map(t=> {
      val tokens = t.split("=")
      expectedSolutionsMap.put(tokens(0).trim, tokens (1).trim.toInt)
    })

    val divergentCells = solution.filter( cell=> {
      val cellValue = cell.valueBitmap.fromBitmask(0)
      val isValueDifferent = expectedSolutionsMap.get(cell.key).get != cellValue
      if(isValueDifferent)
        println(s"Incorrect/Unexpected Cell => ${cell.key} e: ${cell.valueBitmap.fromBitmask(0)} a:${expectedSolutionsMap.get(cell.key).get}")
      isValueDifferent
    })
    assertEquals(81 - expectedSolvedCellCount,  divergentCells.length)
  }
}
