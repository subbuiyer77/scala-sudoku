Feature: Solving sudoku puzzles

  Scenario Outline: Solving puzzles using the naked singles approach
    When cells <givenCells> are certain
    Then then the <solution> has <solvedCellCount> cells
    Examples:
      | givenCells                                                                                                                                       | solvedCellCount | solution                                                                                                                                                                                                                                                                                                                                                                                                             |
      | C4=9,A3=9,H4=3,F6=2,I3=2,D2=2,F3=6,B1=2,C9=7,A8=4,H9=5,G1=1,I2=6,D4=4,D7=3,F8=7,B6=8,A7=8,G6=7,I7=4,E5=7,F4=1,E8=5,H5=6,G9=9,C1=3,B5=5,D6=5,E2=1 | 81              | C4=9,A3=9,H4=3,F6=2,I3=2,D2=2,F3=6,B1=2,C9=7,A8=4,H9=5,G1=1,I2=6,D4=4,D7=3,F8=7,B6=8,A7=8,G6=7,I7=4,E5=7,F4=1,E8=5,H5=6,G9=9,C1=3,B5=5,D6=5,E2=1,F7=9,B8=9,H7=7,A1=6,C7=5,A2=5,I1=7,G3=5,F1=5,D3=7,B2=7,G2=3,I4=5,B4=6,E4=8,B7=1,D5=9,F5=3,G4=2,B9=3,B3=4,G7=6,H3=8,E3=3,C2=8,A4=7,E6=6,D1=8,A9=2,G8=8,E7=2,C3=1,F2=4,A5=1,E9=4,C8=6,A6=3,I9=1,F9=8,G5=4,D8=1,C6=4,E1=9,I5=8,H2=9,D9=6,I6=9,H1=4,I8=3,H6=1,C5=2,H8=2 |
