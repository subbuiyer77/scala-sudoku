Feature: Cell Key transformations

  Scenario Outline: Converting cell key to column, row and square labels
    When noop
    Then <cellKey> yields labels <colLabel>, <rowLabel> and <squareLabel>
    Examples:
      | cellKey | colLabel | rowLabel | squareLabel |
      | A1      | A        | 1        | S00         |
      | E2      | E        | 2        | S01         |
      | I9      | I        | 9        | S22         |

  Scenario Outline: Converting column, row array coordinates to cell key
    When noop
    Then (<rowindex>, <colindex>) coordinates yields <cellKey>
    Examples:
      | rowindex | colindex | cellKey |
      | 0        | 0        | A1      |
      | 0        | 1        | B1      |
      | 8        | 8        | I9      |

  Scenario Outline: Converting cell key to column and row indices
    When noop
    Then <cellKey> yields indices <rowIndex>, <colIndex>
    Examples:
      | cellKey | rowIndex | colIndex |
      | A1      | 0        | 0        |
      | B1      | 0        | 1        |
      | I9      | 8        | 8        |
