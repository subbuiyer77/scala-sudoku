import _root_.sbt.Keys._

name := "scala-sudoku-solver"

version := "1.0"

scalaVersion := "2.10.5"

libraryDependencies ++= Seq(
  "info.cukes" %% "cucumber-scala" % "1.2.2",
  "info.cukes" % "cucumber-junit" % "1.2.2",
  "junit" % "junit" % "4.12"% "test")

resolvers += "Local Maven Repository" at "~/.m2/repository"
